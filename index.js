/* eslint-disable no-undef */
const aws = require('aws-sdk');
const s3 = new aws.S3({ apiVersion: '2006-03-01' });
const simpleParser = require('mailparser').simpleParser;
/**
 * A Lambda function that extract the latest report from s3 bucket, extract csv, and upload to S3 bucket
 */
exports.handler = async (event) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  console.log(`Topic: ${process.env.topic}`);

  try {
    const mail = await getLatestEmail();
    if (!mail) {
      return { success: false, message: 'Couldn\'t get the mail from S3 bucket'};
    }
    const attachment = await parseEmail(mail);
    if (!attachment) {
      return { success: false, message: 'No CSV found'};
    }
    const uploadResponse = await uploadAttachment(attachment);
    return { success: true, message: `Upload report to S3 Bucket ${process.env.dstBucket} successfully`, uploadResponse };
  } catch (err) {
    console.log(err);
    const message = `Error getting the email or parsing the email, please make sure the email exists and the format is correct`;
    console.log(message);
    return { success: false, message: 'Couldn\'t get the mail from S3 bucket'};
  }
};

const getTheLatestObject = (contents) => {
  let currentLastestTime = null;
  let latestObj = null;
  for (const obj of contents) {
    if (obj.Size) {
      // console.log(obj.Key);
      const date = new Date(obj.LastModified);
      if (!currentLastestTime) {
        currentLastestTime = date;
        latestObj = obj;
      } else if (date.getTime() > currentLastestTime.getTime()){
        currentLastestTime = date;
        latestObj = obj;
      }
      // console.log(date);
    }
  }
  return latestObj;
};

const getLatestEmail = async () => {
  const params = {
    Bucket: process.env.srcBucket,
    Prefix: `${process.env.topic}/`
  };
  let objPromise;
  try {
    // Get the list of objects
    objPromise = s3.listObjectsV2(params).promise();
  } catch (err) {
    console.log(err);
    const message = `Error getting the list of objectsfrom bucket ${process.env.srcBucket}. Make sure they exist and your bucket is in the same region as this function.`;
    console.log(message);
    throw new Error(message);
  }

  const mObj = await objPromise.then(data => {
    // Sort out the latest object meta data
    const mailObject = getTheLatestObject(data.Contents);
    console.log(`The latest mail: `);
    console.log(mailObject);
    return mailObject;
  }).catch(error => console.error(error));

  const getObjectParams = {
    Bucket: process.env.srcBucket,
    Key: mObj.Key
  }
  let response;
  try {
  // Retrieve the latest object on S3
    response = await s3.getObject(getObjectParams).promise();
  } catch (err) {
    console.log(err);
    const message = `Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.`;
    console.log(message);
    throw new Error(message);
  }

  return response.Body;
}

const uploadAttachment = async (attachment) => {
  const newFilename = formatFilename(attachment.filename);
  
  const destParams = {
    Bucket: process.env.dstBucket,
    Key: `${process.env.topic}/${newFilename}`,
    Body: attachment.content,
    ContentType: attachment.contentType
  };
  console.log(`Upload to ${process.env.topic}/${newFilename}`);
  return await s3.putObject(destParams).promise();
};

const parseEmail = async (mail) => await simpleParser(mail)
.then(async(mail) => {
  for (const attachment of mail.attachments) {
    if (attachment.contentType === 'text/csv') {
      console.log(`Content Type: ${attachment.contentType}`)
      console.log(typeof(attachment.content));
      console.log(`size: ${attachment.size}`);
      return attachment;
    }
  }
  return;
}).catch(err => {
  console.error(err);
});

const formatFilename = (filename) => {
    return filename.replace(/ /g, '_');
}

// const formatUnit = (unit) => unit.length < 2 ? '0' + unit : unit;
// const getAttachmentPath = () => {
//   let d = new Date(),
//   year = d.getFullYear(),
//   month = formatUnit('' + (d.getMonth() + 1)),
//   day = formatUnit('' + d.getDate()),
//   minute = formatUnit('' + d.getMinutes()),
//   hour = formatUnit('' + d.getHours());

//     return [year, month, day, `${hour}${minute}`].join('-');
// };

// export for unit test
exports.parseEmail = parseEmail;
exports.formatFilename = formatFilename;