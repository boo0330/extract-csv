#! /bin/bash

zip -r9 code.zip . -x '*.git*' '*tests*' 'events*' '*fixture*' code.zip;

aws lambda update-function-code \
--function-name extractCSV \
--zip-file fileb://code.zip