/* eslint-disable no-undef */
'use strict';

const fs = require('fs');

describe('Test sub-functions', () => {
  let mail, csv;
  beforeEach(async () => {
    // Prepare fake email data
    mail = fs.readFileSync('fixture/with-attachment.eml');
    csv = fs.readFileSync('fixture/Status report 20200902040001.csv');
    console.log = jest.fn();
  });
  afterAll(() => {});

  it ('should extract csv from the email', async () => {

    // console.log = jest.fn();

    const parseEmail = require('../../index').parseEmail;
    const attachmnent = await parseEmail(mail);
    expect(attachmnent.contentType).toBe('text/csv');
    expect(attachmnent.filename).toBe('Status report 20200902040001.csv');
    expect(attachmnent.content).toEqual(csv);
  });


  it ('should rename the csv, replace space with dash', () => {
    const formatFilename = require('../../index').formatFilename;
    const originFilename = 'Status report 20200902040001.csv';
    const newFilename = formatFilename(originFilename);

    expect(newFilename).toEqual('Status_report_20200902040001.csv');
  });
});
