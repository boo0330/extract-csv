/* eslint-disable no-undef */
'use strict';

const AWS = require('aws-sdk-mock');
const fs = require('fs');

describe('Test handler', () => {
  const OLD_ENV = process.env;
  let mail, mailWithoutCSV;
  let listObjectsResp, getObjectResp, putObjectResp, event, handler;
  beforeEach(async () => {
    // Prepare fake email data
    mail = fs.readFileSync('fixture/with-attachment.eml');
    mailWithoutCSV = fs.readFileSync('fixture/without-attachment.eml');
    process.env = { ...OLD_ENV, topic: 'status', dstBucket: 'eml-dest', srcBucket: 'eml-src' };
    listObjectsResp = {
      Contents: [
        {
          Key: 'status/',
          LastModified: '2020-08-29T21:21:28.000Z',
          ETag: '"d41d8cd98f00b204e9800998ecf8427e"',
          Size: 0,
          StorageClass: 'STANDARD'
        },
        {
          Key: 'status/08272020.eml',
          LastModified: '2020-08-29T22:02:05.000Z',
          ETag: '"e1f63c1a62e7eb17dbad523d9d590301"',
          Size: 211217,
          StorageClass: 'STANDARD'
        },
        {
          Key: 'status/74qch68r6vmjtmp6gd8s1faqcu3cstmtprv6qko1',
          LastModified: '2020-08-29T22:00:31.000Z',
          ETag: '"446cb360cf04b18e3680fd67c0efd01b"',
          Size: 125757,
          StorageClass: 'STANDARD'
        },
        {
          Key: 'status/attached 1.eml',
          LastModified: '2020-08-29T22:15:32.000Z',
          ETag: '"07e070cfd25fd3cff3020425c852f298"',
          Size: 1127177,
          StorageClass: 'STANDARD'
        }
      ]
    }

    putObjectResp = {
      ETag: '"940acacd47bcf52230650e1995660564"'
    }

    AWS.mock('S3', 'listObjectsV2', function(params, callback) {
      callback(null, listObjectsResp);
    });

    AWS.mock('S3', 'putObject', function(params, callback) {
      callback(null, putObjectResp);
    })

    event = {
      Records: [
        {
          s3: {
            bucket: {
              name: "test-bucket"
            },
            object: {
              key: "test-key"
            }
          }
        }
      ]
    }

    handler = require('../../index.js');
  });
  afterAll(() => {});

  it('should extract csv from latest email', async () => {
    getObjectResp = {
      Body: mail
    };
    AWS.mock('S3', 'getObject', function(params, callback) {
      callback(null, getObjectResp);
    });

    const event = {
      Records: [
        {
          s3: {
            bucket: {
              name: "test-bucket"
            },
            object: {
              key: "test-key"
            }
          }
        }
      ]
    }

    console.info = jest.fn();
    console.log = jest.fn();
    console.error = jest.fn();
    const response = await handler.handler(event, null);
    // expect(console.log).toHaveBeenCalled();
    expect(response).toHaveProperty('success');
    expect(response.success).toBe(true);
  });

  it('should return error if no csv exists in the email', async () => {
    getObjectResp = {
      Body: mailWithoutCSV
    };
    AWS.mock('S3', 'getObject', function(params, callback) {
      callback(null, getObjectResp);
    });
    const response = await handler.handler(event, null);
    expect(response).toHaveProperty('success');
    expect(response.success).toBe(false);
  })

  it('should return error if it\'s not in email format', async() => {
    getObjectResp = {
      Body: "This is just a text in file"
    };
    AWS.mock('S3', 'getObject', function(params, callback) {
      callback(null, getObjectResp);
    });
    const response = await handler.handler(event, null);
    expect(response).toHaveProperty('success');
    expect(response.success).toBe(false);
  });

  it('should return error if no messages in the S3 bucket', async() => {
    listObjectsResp = {
      Contents: []
    }

    AWS.mock('S3', 'listObjectsV2', function(params, callback) {
      callback(null, listObjectsResp);
    });
    const response = await handler.handler(event, null);
    expect(response).toHaveProperty('success');
    expect(response.success).toBe(false);
  });
});
