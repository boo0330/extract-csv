# extract-csv

A aws lambda function to read an email from S3 bucket, extract csv, and save to S3.

## AWS lambda environment
Before building and deploying to aws lambda, please create lambda functions on aws lambda with the environment variables below:

- **srcBucket**: the source S3 bucket name for storing email messages
- **dstBucket**: the destination S3 bucket name for saving the extracted csv file
- **topic**: the type of the report, **status** / **activemachinelist**

## Prepare for deployment
- **source S3 bucket for test/staging/production**
- **destination S3 bucket for test/staging/production**
- **aws IAM role full access to lambda for test/staging/production function**
- geneate the access key and secret for the created role, and add it into bitbucket pipeline's **Repository variables** with variables **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY**

## dev - test - deployment flow
```
development -> unit test -> commit and merge -> automatically deploy to test environment 

-> test on test environment -> manually deploy to staging site -> test on staging site -> manually to production -> post release test
```